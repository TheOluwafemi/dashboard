import { Component, OnInit } from '@angular/core';
import {Color} from 'ng2-charts/ng2-charts';

@Component({
  selector: 'app-all-customers',
  templateUrl: './all-customers.component.html',
})
export class AllCustomersComponent implements OnInit {


  public customers = [
    { id: '1', Avatar: 'image1', name: 'Damilola Oluwafemi', company: 'Fidelity Bank', status: 'active', targetDue: '12 Dec', target: 70 },
    { id: '2', Avatar: 'image1', name: 'Simpa Yekini', company: 'Fidelity Bank', status: 'active', targetDue: '12 Dec', target: 70 },
    { id: '50', Avatar: 'image1', name: 'Chuzzy Uzoma', company: 'Fidelity Bank', status: 'active', targetDue: '12 Dec', target: 70 },
    { id: '50', Avatar: 'image1', name: 'Gbemiga Joseph', company: 'Fidelity Bank', status: 'active', targetDue: '12 Dec', target: 70 },
    { id: '50', Avatar: 'image1', name: 'Damilola Oluwafemi', company: 'Fidelity Bank', status: 'active', targetDue: '12 Dec', target: 70 },
    { id: '50', Avatar: 'image1', name: 'Simpa Yekini', company: 'Fidelity Bank', status: 'active', targetDue: '12 Dec', target: 70 },
    { id: '50', Avatar: 'image1', name: 'Chuzzy Uzoma', company: 'Fidelity Bank', status: 'active', targetDue: '12 Dec', target: 70 },
    { id: '50', Avatar: 'image1', name: 'Gbemiga Joseph', company: 'Fidelity Bank', status: 'active', targetDue: '12 Dec', target: 70 },
    { id: '50', Avatar: 'image1', name: 'Damilola Oluwafemi', company: 'Fidelity Bank', status: 'active', targetDue: '12 Dec', target: 70 },
    { id: '50', Avatar: 'image1', name: 'Simpa Yekini', company: 'Fidelity Bank', status: 'active', targetDue: '12 Dec', target: 70 },
    { id: '50', Avatar: 'image1', name: 'Chuzzy Uzoma', company: 'Fidelity Bank', status: 'active', targetDue: '12 Dec', target: 70 },
    { id: '50', Avatar: 'image1', name: 'Gbemiga Joseph', company: 'Fidelity Bank', status: 'active', targetDue: '12 Dec', target: 70 }
  ];

  ngOnInit() {
  }

}
